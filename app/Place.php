<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'type',
        'price',
        'price_sell',
        'price_location',
        'price_season',
        'area',
        'nbedrooms',
        'nsuites',
        'nbathrooms',
        'nrooms',
        'ngarages',
        'address_1',
        'number',
        'address_2',
        'neighborhood',
        'city',
        'state',
        'country',
    ];
    protected $guarded = ['id', 'created_at', 'update_at', 'deleted_at'];
    protected $table = 'places';
}
