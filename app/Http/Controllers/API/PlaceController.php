<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Http\Controllers\Controller;
use App\Place;

class PlaceController extends Controller
{

    public function index()
    {
        $places = Place::orderBy('created_at', 'desc')->paginate(10);
        return response()->json(['places' => $places]);
    }

    public function store(Request $request)
    {

        $place = new Place;

        $place->code            = $request->code;
        $place->name            = $request->name;
        $place->type            = $request->type;
        $place->price           = $request->price;
        $place->price_sell      = $request->price_sell;
        $place->price_location  = $request->price_location;
        $place->price_season    = $request->price_season;
        $place->area            = $request->area;
        $place->nbedrooms       = $request->nbedrooms;
        $place->nsuites         = $request->nsuites;
        $place->nbathrooms      = $request->nbathrooms;
        $place->nrooms          = $request->nrooms;
        $place->ngarages        = $request->ngarages;
        $place->description     = $request->description;
        $place->image           = $request->image;
        $place->address_1       = $request->address_1;
        $place->number          = $request->number;
        $place->address_2       = $request->address_2;
        $place->neighborhood    = $request->neighborhood;
        $place->city            = $request->city;
        $place->state           = $request->state;
        $place->country         = $request->country;

        $place->save();

        return response()->json($place);
    }

    public function show($id)
    {
        $place = Place::findOrFail($id);
        return response()->json($place);
    }

    public function search($id)
    {
        $place = Place::where('id', '=', $id)
        ->orWhere('code', '=', $id)
        ->firstOrFail();
        return response()->json($place);

    }

    public function update(Request $request, $id)
    {

        $place = Place::findOrFail($id);

        $place->code            = $request->code;
        $place->name            = $request->name;
        $place->type            = $request->type;
        $place->price           = $request->price;
        $place->price_sell      = $request->price_sell;
        $place->price_location  = $request->price_location;
        $place->price_season    = $request->price_season;
        $place->area            = $request->area;
        $place->nbedrooms       = $request->nbedrooms;
        $place->nsuites         = $request->nsuites;
        $place->nbathrooms      = $request->nbathrooms;
        $place->nrooms          = $request->nrooms;
        $place->ngarages        = $request->ngarages;
        $place->description     = $request->description;
        $place->image           = $request->image;
        $place->address_1       = $request->address_1;
        $place->number          = $request->number;
        $place->address_2       = $request->address_2;
        $place->neighborhood    = $request->neighborhood;
        $place->city            = $request->city;
        $place->state           = $request->state;
        $place->country         = $request->country;

        $place->save();

        return response()->json($place);


    }

    public function destroy($id)
    {
        $place = Place::findOrFail($id);
        $place->delete();

        return response()->json(true);
    }

}
