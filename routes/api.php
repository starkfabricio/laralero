<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'API\UserController@register');
Route::post('login', [ 'as' => 'login', 'uses' => 'API\UserController@login']);



Route::get('places', 'API\PlaceController@index');
Route::get('places/{id}', 'API\PlaceController@show');
Route::get('places/search/{id}', 'API\PlaceController@search');
Route::post('places', 'API\PlaceController@store');
Route::patch('places/{id}', 'API\PlaceController@update');
Route::delete('places/{id}', 'API\PlaceController@destroy');



Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');




});