<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('code')->nullable()->unique();
            $table->string('name', 50);
            $table->string('type', 30);
            $table->longText('description');
            $table->decimal('price', 5, 2)->default(0);
            $table->decimal('price_sell', 5, 2)->nullable();
            $table->decimal('price_location', 5, 2)->nullable();
            $table->decimal('price_season', 5, 2)->nullable();
            $table->decimal('area', 5, 2)->default(0);
            $table->integer('nbedrooms')->default(0)->nullable();
            $table->integer('nsuites')->default(0)->nullable();
            $table->integer('nbathrooms')->default(0)->nullable();
            $table->integer('nrooms')->default(0)->nullable();
            $table->integer('ngarages')->default(0)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('address_1', 50)->nullable();
            $table->integer('number')->nullable();
            $table->string('address_2', 50)->nullable();
            $table->string('neighborhood', 50)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('state', 2)->nullable();
            $table->string('country', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
